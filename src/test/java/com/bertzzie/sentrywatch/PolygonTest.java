package com.bertzzie.sentrywatch;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class PolygonTest {

    /**
     * Test getLowestPoint by giving only 1 clear
     * lowest point here.
     *
     * The lowest point is 2.0, 1.0 in which x and y
     * is the lowest point.
     *
     * @throws Exception
     */
    @Test
    public void testGetLowestPoint() throws Exception {
        List<Coord> points = new ArrayList<Coord>();

        points.add(new Coord(10.0f, 10.0f));
        points.add(new Coord(3.0f, 4.0f));
        points.add(new Coord(2.0f, 1.0f)); // lowest point
        points.add(new Coord(13.0f, 12.0f));
        points.add(new Coord(20.0f, 40.0f));

        Coord lowestPoint = Polygon.getLowestPoint(points);

        assertEquals(lowestPoint.X, 2.0f, 0.001f);
        assertEquals(lowestPoint.Y, 1.0f, 0.001f);
    }

    /**
     * Test to see what happened when we have more than 1
     * lowest Y. Should give us the lowest X within all that
     * points.
     *
     * We test only with 2 conflicting Ys for now.
     *
     * @throws Exception
     */
    @Test
    public void testGetLowestPoint2() throws Exception {
        List<Coord> points = new ArrayList<Coord>();

        points.add(new Coord(10.0f, 10.0f));
        points.add(new Coord(3.0f, 1.0f));
        points.add(new Coord(2.0f, 1.0f)); // lowest point
        points.add(new Coord(13.0f, 12.0f));
        points.add(new Coord(20.0f, 40.0f));

        Coord lowestPoint = Polygon.getLowestPoint(points);

        assertEquals(lowestPoint.X, 2.0f, 0.001f);
        assertEquals(lowestPoint.Y, 1.0f, 0.001f);
    }

    /**
     * Test to see what happened when we have more than 1
     * lowest Y AND lowest Y.
     *
     * It shouldn't matter which point is taken, as long as
     * the method don't crash.
     *
     * @throws Exception
     */
    @Test
    public void testGetLowestPoint3() throws Exception {
        List<Coord> points = new ArrayList<Coord>();

        points.add(new Coord(10.0f, 10.0f));
        points.add(new Coord(1.0f, 1.0f)); // lowest point
        points.add(new Coord(1.0f, 1.0f)); // also lowest point
        points.add(new Coord(1.0f, 1.0f)); // another lowest point
        points.add(new Coord(20.0f, 40.0f));

        Coord lowestPoint = Polygon.getLowestPoint(points);

        assertEquals(lowestPoint.X, 1.0f, 0.001f);
        assertEquals(lowestPoint.Y, 1.0f, 0.001f);
    }

    @Test
    public void testFromCoords() throws Exception {
        List<Coord> coords = new ArrayList<Coord>();
        coords.add(new Coord(10.0f, 6.0f));
        coords.add(new Coord(-2.0f, 4.0f));
        coords.add(new Coord(7.0f, 3.0f));
        coords.add(new Coord(-3.0f, -4.0f));
        coords.add(new Coord(14.0f, -5.0f));

        Polygon poly = Polygon.fromCoords(coords);

        List<Coord> expected = new ArrayList<Coord>();
        expected.add(new Coord(14.0f, -5.0f));
        expected.add(new Coord(10.0f, 6.0f));
        expected.add(new Coord(7.0f, 3.0f));
        expected.add(new Coord(-2.0f, 4.0f));
        expected.add(new Coord(-3.0f, -4.0f));
        expected.add(new Coord(14.0f, -5.0f));

        assertEquals(poly.getPoints(), expected);
    }

    @Test
    public void testFromCoords2() throws Exception {
        List<Coord> coords = new ArrayList<Coord>();
        coords.add(new Coord(0.0f, 10.0f));
        coords.add(new Coord(10.0f, 0.0f));
        coords.add(new Coord(10.0f, 10.0f));
        coords.add(new Coord(0.0f, 0.0f));

        Polygon poly = Polygon.fromCoords(coords);

        List<Coord> expected = new ArrayList<Coord>();
        expected.add(new Coord(0.0f, 0.0f));
        expected.add(new Coord(10.0f, 0.0f));
        expected.add(new Coord(10.0f, 10.0f));
        expected.add(new Coord(0.0f, 10.0f));
        expected.add(new Coord(0.0f, 0.0f));

        assertEquals(poly.getPoints(), expected);
    }

    @Test
    public void testCircumference() throws Exception {
        List<Coord> coords = new ArrayList<Coord>();
        coords.add(new Coord(0.0f, 10.0f));
        coords.add(new Coord(10.0f, 0.0f));
        coords.add(new Coord(10.0f, 10.0f));
        coords.add(new Coord(0.0f, 0.0f));

        Polygon poly = Polygon.fromCoords(coords);
        double circ  = poly.getCircumference();

        assertEquals(40.0f, circ, 0.001f);
    }

    @Test
    public void testArea() throws Exception {
        List<Coord> coords = new ArrayList<Coord>();
        coords.add(new Coord(0.0f, 10.0f));
        coords.add(new Coord(10.0f, 0.0f));
        coords.add(new Coord(10.0f, 10.0f));
        coords.add(new Coord(0.0f, 0.0f));

        Polygon poly = Polygon.fromCoords(coords);
        double area  = poly.getArea();

        assertEquals(100.0f, area, 0.001f);
    }
}