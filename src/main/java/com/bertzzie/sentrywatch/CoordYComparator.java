package com.bertzzie.sentrywatch;

import java.util.Comparator;

/**
 * Created by bert on 12/07/2014.
 */
public class CoordYComparator implements Comparator<Coord> {
    private Coord lowestPoint;

    public CoordYComparator(Coord lowestPoint) {
        this.lowestPoint = lowestPoint;
    }

    @Override
    public int compare(Coord c1, Coord c2) {
        int compX = Float.compare(c1.X, c2.X);
        int compY = Float.compare(c1.Y, c2.Y);

        if (compX == 0 && compY == 0) {
            return 0;
        }

        double tethaA = Math.atan2(c1.Y - lowestPoint.Y, c1.X - lowestPoint.X);
        double tethaB = Math.atan2(c2.Y - lowestPoint.Y, c2.X - lowestPoint.X);

        int compTetha = Double.compare(tethaA, tethaB);
        if (compTetha < 0) {
            return -1;
        } else if (compTetha > 0) {
            return 1;
        } else {
            double distA = Math.sqrt(Math.pow(lowestPoint.X - c1.X, 2) + Math.pow(lowestPoint.Y - c1.Y, 2));
            double distB = Math.sqrt(Math.pow(lowestPoint.X - c2.X, 2) + Math.pow(lowestPoint.Y - c2.Y, 2));

            int compDist = Double.compare(distA, distB);
            if (compDist < 0) {
                return -1;
            } else {
                return 1;
            }
        }
    }
}
