package com.bertzzie.sentrywatch;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

/**
 * Created by bert on 12/07/2014.
 */
public class Polygon {
    private List<Coord> Points;

    private Polygon() {};

    public List<Coord> getPoints() {
        return this.Points;
    }

    public void setPoints(List<Coord> points) {
        this.Points = points;
    }

    public double getCircumference() {
        double total = 0.0;

        for (int i = 0; i < Points.size() - 1; i++) {
            total = total + Coord.getDistance(Points.get(i), Points.get(i + 1));
        }

        return total;
    }

    public double getArea() {
        double total = 0.0;

        for (int i = 1; i < Points.size() - 1; i++) {
            total = total + ((Points.get(i-1).X + Points.get(i).X) * (Points.get(i-1).Y - Points.get(i).Y));
        }

        return Math.abs(total / 2);
    }

    protected static Coord getLowestPoint(List<Coord> points) {
        Coord lowest = points.get(0);

        for (int i = 1; i < points.size(); i++) {
            Coord cur = points.get(i);
            int comp  = Float.compare(cur.Y, lowest.Y);

            if (comp <= 0 || Float.compare(cur.X, lowest.Y) <= 0) {
                lowest = cur;
            }
        }

        return lowest;
    }

    public static Polygon fromCoords(List<Coord> points) {
        // defensive copy.
        List<Coord> sorted = points;
        Coord lowestPoint  = getLowestPoint(sorted);

        Collections.sort(sorted, new CoordYComparator(lowestPoint));

        sorted.add(sorted.get(0));

        Polygon polygon = new Polygon(); // result
        polygon.setPoints(sorted);

        return polygon;
    }
}

