package com.bertzzie.sentrywatch;

import java.util.*;

/**
 * Created by bert on 04/06/2014.
 */
public class GameData {
    public Integer MatchID;
    public Integer GameState;
    public Double GameTime;

    public Integer RadiantNetWorth;
    public Integer DireNetWorth;

    public Integer RadiantExp;
    public Integer DireExp;

    public Coord[] HeroPosition;

    private GameData() {}

    public GameData(int HeroCount) {
        HeroPosition = new Coord[10];
    }

    public String toString() {
        String f = "%d;%f;%d;%d;%d;%d;%d";
        String output;

        output = String.format(Locale.US, f, MatchID, GameTime, GameState, RadiantNetWorth, DireNetWorth, RadiantExp, DireExp);
        for (int i = 0; i < HeroPosition.length; i++) {
            output = output + String.format(Locale.US, ";(%.2f,%.2f)", HeroPosition[i].X, HeroPosition[i].Y);
        }

        return output;
    }

    public String toCompleteString() {
        String f = "%d;%.0f;%d;%d;%d;%d;%d;%d;%d;";
        String output;

        output = String.format(Locale.US, f,
                MatchID,
                GameTime,
                GameState,
                RadiantNetWorth,
                DireNetWorth,
                RadiantNetWorth - DireNetWorth,
                RadiantExp,
                DireExp,
                RadiantExp - DireExp);

        // we store all hero pos as polygon in postgis
        // format is the following: POLYGON((x1 y1, x2 y2, ... , xn yn))
        // also: last point of the polygon must be also first point since it's enclosed
        // this is why we started with a 'POLYGON((' and will end it with 'x1, y1))'
        output = output + "ST_GeomFromText(POLYGON((";
        for (int i = 0; i < HeroPosition.length; i++) {
            output = output + String.format(Locale.US, "%d %d,", Math.round(HeroPosition[i].X), Math.round(HeroPosition[i].Y));
        }

        // remove the last comma and add closed parenthesis
        // see the comment before
        output = output.substring(0, output.length() - 1);
        output = output + Math.round(HeroPosition[0].X) + Math.round(HeroPosition[0].Y) + ")))";

        // Circumference and Area of Hero Pos' polygon
//        Polygon poly = Polygon.fromCoords(Arrays.asList(HeroPosition));
        Polygon poly = Polygon.fromCoords(new ArrayList<Coord>(Arrays.asList(HeroPosition)));
        double area  = poly.getArea();
        double circ  = poly.getCircumference();

        output = output + String.format(Locale.US, ";%.0f;%.0f", area, circ);

        return output;
    }

    public String toPostGISString() {
        String f = "%d;%.0f;%d;%d;%d;%d;%d;";
        String output;

        output = String.format(Locale.US, f, MatchID, GameTime, GameState, RadiantNetWorth, DireNetWorth, RadiantExp, DireExp);

        // we store all hero pos as polygon in postgis
        // format is the following: POLYGON((x1 y1, x2 y2, ... , xn yn))
        // also: last point of the polygon must be also first point since it's enclosed
        // this is why we started with a 'POLYGON((' and will end it with 'x1, y1))'
        output = output + "ST_GeomFromText(POLYGON((";
        for (int i = 0; i < HeroPosition.length; i++) {
            output = output + String.format(Locale.US, "%d %d,", Math.round(HeroPosition[i].X), Math.round(HeroPosition[i].Y));
        }

        // remove the last comma and add closed parenthesis
        // see the comment before
        output = output.substring(0, output.length() - 1);
        output = output + Math.round(HeroPosition[0].X) + Math.round(HeroPosition[0].Y) + ")))";

        return output;
    }

    public String toPostgresString() {
        String f = "%d;%f;%d;%d;%d;%d;%d;";
        String output;

        output = String.format(Locale.US, f, MatchID, GameTime, GameState, RadiantNetWorth, DireNetWorth, RadiantExp, DireExp);

        // we store all hero pos as polygon in postgres
        // format is the following: ((x1, y1), (x2, y2), ... , (xn, yn))
        // this is why we started with a '(' and will end it with ')'
        output = output + "(";
        for (int i = 0; i < HeroPosition.length; i++) {
            output = output + String.format(Locale.US, "(%.2f,%.2f),", HeroPosition[i].X, HeroPosition[i].Y);
        }

        // remove the last comma and add closed parenthesis
        // see the comment before
        output = output.substring(0, output.length() - 1);
        output = output + ")";

        return output;
    }

    public Map toMap() {
        Map<String, Object> result = new HashMap<String, Object>();

        result.put("MatchID", this.MatchID);
        result.put("GameTime", this.GameTime);
        result.put("GameState", this.GameState);
        result.put("RadiantNetWorth", this.RadiantNetWorth);
        result.put("DireNetWorth", this.DireNetWorth);
        result.put("RadiantExp", this.RadiantExp);
        result.put("DireExp", this.DireExp);

        for (int i = 0; i < HeroPosition.length; i++) {
            result.put(String.format("Hero%dXPos", i), HeroPosition[i].X);
            result.put(String.format("Hero%dYPos", i), HeroPosition[i].Y);
        }

        return result;
    }

    @Override
    public boolean equals(Object object) {
        int retval = 1;

        if (object != null && object instanceof GameData) {
            retval = Double.compare(this.GameTime, ((GameData)object).GameTime);
        }

        return retval == 0;
    }
}
