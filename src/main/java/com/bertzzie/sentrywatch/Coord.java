package com.bertzzie.sentrywatch;

import skadistats.clarity.model.Entity;

import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;

/**
 * Created by bert on 01/06/2014.
 */
public class Coord {
    public Float X;
    public Float Y;

    private static final int MAX_COORD_INTEGER = 16384;

    public Coord(Float x, Float y) {
        this.X = x;
        this.Y = y;
    }

    public static double getDistance(Coord point1, Coord point2) {
        return Math.sqrt(((point2.X - point1.X) * (point2.X - point1.X)) +
                          (point2.Y - point1.Y) * (point2.Y - point1.Y));
    }

    public static Coord fromHeroEntity(Entity hero) {
        float x = Coord.getHeroXPos(hero);
        float y = Coord.getHeroYPos(hero);

        return new Coord(x, y);
    }

    public static float getVecOrigin(Entity e, int idx) {
        Object v = e.getProperty("m_vecOrigin");
        if (v instanceof Vector2f) {
            float[] v2 = new float[2];
            ((Vector2f) v).get(v2);
            return v2[idx];
        } else if (v instanceof Vector3f) {
            float[] v3 = new float[3];
            ((Vector3f) v).get(v3);
            return v3[idx];
        } else {
            throw new RuntimeException("unsupported vector found");
        }
    }

    public static float getHeroXPos(Entity e) {
        int cellBits = e.getProperty("m_cellbits");
        int cellX = e.getProperty("m_cellX");
        return cellX * (1 << cellBits) - MAX_COORD_INTEGER + getVecOrigin(e, 0) / 128.0f;
    }

    public static float getHeroYPos(Entity e) {
        int cellBits = e.getProperty("m_cellbits");
        int cellY = e.getProperty("m_cellY");
        return cellY * (1 << cellBits) - MAX_COORD_INTEGER + getVecOrigin(e, 1) / 128.0f;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Coord coord = (Coord) o;

        if (!X.equals(coord.X)) return false;
        if (!Y.equals(coord.Y)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = X.hashCode();
        result = 31 * result + Y.hashCode();
        return result;
    }
}
