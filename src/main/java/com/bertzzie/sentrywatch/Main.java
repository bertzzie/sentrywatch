package com.bertzzie.sentrywatch;

import skadistats.clarity.Clarity;
import skadistats.clarity.match.Match;
import skadistats.clarity.model.*;
import skadistats.clarity.parser.DemoInputStreamIterator;
import skadistats.clarity.parser.Profile;

import skadistats.clarity.match.EntityCollection;

import com.dota2.proto.Demo.CDemoFileInfo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Main {
    private static String TotalEarnedGoldProp = "EndScoreAndSpectatorStats.m_iTotalEarnedGold.000%d";
    private static String TotalExpProp = "EndScoreAndSpectatorStats.m_iTotalEarnedXP.000%d";
    private static String SelectedHeroProp = "m_hSelectedHero.000%d";

    private static final int MAX_HERO = 10;

    public static Integer TotalNetWorth (Entity ent, String team) {
        Integer total = 0;
        int lb, ub;
        if ("Radiant" == team) {
            lb = 0;
            ub = 4;
        } else {
            lb = 5;
            ub = 9;
        }

        for (;lb <= ub; lb++) {
            total = total + ((Integer) ent.getProperty(String.format(TotalEarnedGoldProp, lb)));
        }

        return total;
    }

    public static Integer TotalEXP (Entity ent, String team) {
        Integer total = 0;
        int lb, ub;

        if ("Radiant" == team) {
            lb = 0;
            ub = 4;
        } else {
            lb = 5;
            ub = 9;
        }

        for (; lb <= ub; lb++) {
            total = total + ((Integer) ent.getProperty(String.format(TotalExpProp, lb)));
        }

        return total;
    }

    public static void PrintProperties (DTClass dtc) {
        List<ReceiveProp> rp = dtc.getReceiveProps();
        int dtcs = rp.size();
        for (int i = 0; i < dtcs; i++) {
            ReceiveProp r = rp.get(i);
            System.out.printf("%s %s\n", r.getDtName(), r.getVarName());
        }
    }

    public static List<GameData> ProcessReplay(String path) throws Exception {
        List<GameData> gameDatas = new ArrayList<GameData>();

        Match match = new Match();
        DemoInputStreamIterator iter = Clarity.iteratorForFile(path, Profile.ALL);

        CDemoFileInfo gameInfo = Clarity.infoForFile(path);
        Integer matchID = gameInfo.getGameInfo().getDota().getMatchId();

        // Main Data Gaher Loop Starts
        while (iter.hasNext()) {
            GameData newData = new GameData(MAX_HERO);
            newData.MatchID = matchID;

            iter.next().apply(match);

            EntityCollection ec = match.getEntities();

            // Game Time
            Iterator<Entity> grp = ec.getAllByDtName("DT_DOTAGamerulesProxy");
            while (grp.hasNext()) {
                Entity e = grp.next();
                Float gtime = ((Float)e.getProperty("dota_gamerules_data.m_fGameTime")) -
                        ((Float)e.getProperty("dota_gamerules_data.m_flGameStartTime")) / 60;
                Integer gstate = e.getProperty("dota_gamerules_data.m_nGameState");
                newData.GameTime = Math.floor(gtime);
                newData.GameState = gstate;
            }

            // Net worth
            Iterator<Entity> ess = ec.getAllByDtName("DT_DOTA_PlayerResource");
            while (ess.hasNext()) {
                Entity e = ess.next();

                // Net Worth Start
                Integer rNW = TotalNetWorth(e, "Radiant");
                Integer dNW = TotalNetWorth(e, "Dire");

                newData.RadiantNetWorth = rNW;
                newData.DireNetWorth = dNW;
                // Net Worth End

                // EXP Start
                Integer rXP = TotalEXP(e, "Radiant");
                Integer dXP = TotalEXP(e, "Dire");

                newData.RadiantExp = rXP;
                newData.DireExp = dXP;
                // EXP End

                // Player Coordinate Start
                for (int i = 0; i < MAX_HERO; i++) {
                    Integer heroHandle = e.getProperty(String.format(SelectedHeroProp, i));

                    // has not picked yet.
                    if (heroHandle != Handle.MAX) {
                        Entity hero = ec.getByHandle(heroHandle);
                        // game not starting yet (hero is not loaded)
                        if (hero != null) {
                            newData.HeroPosition[i] = Coord.fromHeroEntity(hero);
                        } else {
                            newData.HeroPosition[i] = new Coord(0.0f, 0.0f);
                        }
                    } else {
                        newData.HeroPosition[i] = new Coord(0.0f, 0.0f);
                    }
                }

                if (!gameDatas.contains(newData)) {
                    gameDatas.add(newData);
                }
            }
        }

        return gameDatas;
    }

    public static void PrintReplayData(List<GameData> datas, boolean withHeader) {
        String header = "MatchID;GameTime;GameState;RNW;DNW;NWDIFF;RXP;DXP;XPDIFF;HeroPos;Area;Circumference";

        if (withHeader) {
//        System.out.println("MatchID;GameTime;GameState;RNW;DNW;RXP;DXP;H1Pos;H2Pos;H3Pos;H4Pos;H5Pos;H6Pos;H7Pos;H8Pos;H9Pos;H10Pos");
            System.out.println(header);
        }

        for (GameData data: datas) {
            System.out.println(data.toCompleteString());
        }
    }

    public static void main (String[] args) throws Exception {
        String replayFilePath = args[0];

//        try {
            List<GameData> gameDatas = ProcessReplay(replayFilePath);
            PrintReplayData(gameDatas, true);
//        } catch (Exception e) {
//            System.out.println("File read fail");
//        }
    }
}
